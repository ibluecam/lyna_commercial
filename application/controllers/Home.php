<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Dompdf\Dompdf;
class Home extends My_Controller {

	public function __construct(){
		parent::__construct();
	} 
	public function index()
	{
			
		$root = "http://".$_SERVER['HTTP_HOST'];
		$content = file_get_contents($root."/home/testpdf");
		if($this->input->post("test")){
			// instantiate and use the dompdf class
			$dompdf = new Dompdf();
			$dompdf->loadHtml($content);

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'landscape');

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream();
		}
		$this->data['home'] = "This is home page";
		$this->load_view('publics/home');
		// reference the Dompdf namespace
	}
	public function testpdf(){
		$this->load_view('testpdf');
	}
}
