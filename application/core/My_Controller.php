<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	// declare variable	
	public $data = array();
	var $array_css = array();
	var $array_js = array();
	public function __construct()
	{
		parent::__construct();				
	}
	
	public function load_view($view_file){
		$this->data['array_css'] = $this->array_css;
		$this->data['array_js'] = $this->array_js;
		$this->load->view('/templates/header', $this->data);
		$this->load->view($view_file, $this->data);
		$this->load->view('/templates/footer', $this->data);
	}
}

?>